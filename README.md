# Line Server

This application is a solution to the "Line Server Problem".  It is a network server that serves
individual lines out of a file to network clients using a simple REST API.

`GET /lines/<line index>`

* Returns an HTTP status of 200 and the text of the requested line or an HTTP 413 status if the requested line is beyond the end of the file. Line numbers are 1 indexed, so `GET /lines/1` returns the first line in the file.

`GET /lines/stats`

* Returns some basic performance statistics about line number requests since the app was started.

There are many ways to implement this which are trivial - e.g. read all the lines in the file into a database - but this goes against the spirit of the exercise.   For  the sake of the exercise, we assume the following:

1. The file cannot be changed.
2. It is a database-free environment.
3. Auxiliary files can be created but should be small relative to the original file.

## Installation and Getting Started

1. Clone the repo from Bitbucket.
2. Ensure you have Java and Maven installed locally or build will not succeed.
3. Run `build.sh` from root of repo.  (Does a `mvn clean install`)
4. Locate an existing text file on your machine or use the system to generate on.  (See details on TextFileGenerator.java below)
5. Run `run.sh [path_to_text_file]` from root of repo 
6. In a browser, access `http://localhost:8080/`.  You should get a hello message. 
7. Then access `http://localhost:8080/lines/[line_number]` to get a requested line from the file.

## How does the system work?

The heart of the system is an interface for objects which can extract lines from a text file:

```java
    abstract class LineReader
    {
        public abstract String getLine(int lineNumber) throws ArrayIndexOutOfBoundsException, IOException;
    }
```

Note that the abstract `getLine(int)` is one indexed, so to retrieve the first line in the file, pass a 1 not a 0!
Also note that the API uses an int parameter so that the maximum number of 
supported lines in a file is 2,147,483,647.

System provides several implementation classes for this interface, progressing from the simple (i.e. naive) to the (slightly more) 
sophisticated.  None of these implementations is perfect and appropriate for all text files and all
environments. We summarize the classes and their tradeoffs below:

1. **SimpleMemoryLineReader**:  Reads in the entire file at construction time, parses into lines, and stores the lines in an in-memory ArrayList.  At request time, the requested line is served directly.  Can easily eat up all memory because all the lines are stored in memory.

2. **SimpleFileLineReader**: Does not read in the entire file at construction time. At request time, it reads from the beginning using Files stream API until it reaches the requested line number and then serves it.  Plays nicer with the memory but extracting text slows down dramatically  for large text files especially when requesting lines near the end of the file.

3. **LineReaderWithMemoryIndex**: At construction time, builds an in-memory ArrayList of the positions of all newline characters in the text. At request time, this array is used  to determine the position of the requested line in the text file.  Text is then extracted using the RandomAccessFile API. Generally a nice solution.  Does not hold the lines themselves in memory, simply a list of Integers.  But this too can consume all the heap space.  On my local machine (using the default 2GB max heap space), this caused an OOM when the list reached size 70,090,000 integers/lines.

4. **LineLeaderWithFileIndex**:  Similar to FileLineReaderWithMemoryIndex but instead of using an in-memory list to store the newline positions, it writes them to a second "index" file.  The lines in this index file have fixed length and so any line can be efficiently read.  Accessing both files is performed with the RandomAcessFile API. Solution provides very rapid line extraction even for very large files.  However, it pollutes the disk with an index file.  In the worst case this index file can be even bigger than the original file!  Furthermore, building the index takes time.  One can expect ~ 1 minute per 2GB file in building this index.

5. **LineLeaderWithSignposts**:  Similar to FileLineReaderWithMemoryIndex but only records the position of every Nth newline character (where N=1000)

Because none of these implementations is perfect, we use a simple heuristic to determine which one to use for a given input file. This logic is contained inside  `LineReaderFactory.getLineReader(File file)`.  This method will return a simpler in-memory implementation for any text files less than 1GB and `FileLineLeaderWithFileIndex` for any file 1GB or larger.

The web application itself is a simple SpringBoot application.  Therefore, at build time
the result is a fat jar file.  The entry point to the application is `com.craigmilley.lineserver.app.LinesServer --file [path_to_file]`.  This class creates the appropriate reader class and an starts the webapp.  The class `LinesController`  handles the REST API calls and is a traditional Spring Web RestController.

## How does the system perform with varying file sizes?

To get a sense for how the system performs, we did some simple performance testing.
The testing is done at the internal API level and not through the REST/web interface.
The system provides a couple quick and dirty classes for generating large test data files and then running a benchmark 
test.  

The syntax to generate the test files and run benchmarks is:

```
java com.craigmilley.lineserver.util.TextFileGenerator

java com.craigmilley.lineserver.util.Benchmark \
    --file "test-data/LargeTestFileWithLongerLines.txt" \
    --reader "com.craigmilley.lineserver.reader.LineReaderWithFileIndex" \
    --trials 100 \
    --lines 4000000
```

The results we saw when we ran on local machine:

| File                             |     Size |       Lines | Index Size | Index Time |         Avg |   Min |      Max |
|----------------------------------|---------:|------------:|------------|-----------:|------------:|------:|---------:|
| MediumTestFile.txt               | 548.9 MB |   1,000,000 |        N/A |   9,540 ms |     1.02 ms |  1 ms |     2 ms |
| LargeTestFile.txt                |  2.29 GB | 100,000,000 |     1.1 GB |  95,625 ms |     0.95 ms |  0 ms |     8 ms |
| LargeTestFileWithLongerLines.txt |   2.2 GB |   4,000,000 |      44 MB |  46,259 ms |     1.80 ms |  1 ms |     6 ms |

Note that these results are good:  Less than 2 ms for extracting text from large text file!  With naive "read from beginning" strategy, we saw the following much worse times

| File                             |     Size |       Lines | Index Size | Index Time |         Avg |   Min |      Max |
|----------------------------------|---------:|------------:|------------|-----------:|------------:|------:|---------:|
| MediumTestFile.txt               | 548.9 MB |   1,000,000 |        N/A |        N/A |   392.35 ms | 17 ms |   790 ms |
| LargeTestFile.txt                | 2.29 GB  | 100,000,000 |        N/A |        N/A | 2,652.24 ms | 61 ms | 5,516 ms |


I did not do testing with larger files but we can predict the following.  Files up to 1 GB file will perform extremely well (< 1 ms per line).  Files of size 10 GB and 100 GB file should also perform very well (< 20 msg per line).  Performance is in general limited to performance of Java's RandomAccessFile.seek().  The indexing time will be approximately 1 minute per 2 GB (so could be close to an hour for 100GB file), and this should be considered in the overall performance times.  The size of index dramatically depends on average line length
 
## How does the system perform with varying number of users?

I did not do any concurrency or scalability testing so can only make general statements.  In general, the number of 
threads allocated by the appserver determines how many threads/processes will be attempting to access
the text file simultaneously.  Since all accesses are read-only, I do not foresee problem with operating
system or number of file handles.  I believe that if response times remain within 0-2 ms, system could handle several
thousand concurrent users.  Would need to test before I would make a larger statement.

## What documentation, websites, papers, etc did I consult in doing this assignment?

I poked around Google and StackOverflow to find articles relevant to the topic.  In generally, I did not
find much useful.  There were many questions about how to optimally read a text file:

* https://stackoverflow.com/questions/5868369/how-to-read-a-large-text-file-line-by-line-using-java/49713184
* https://stackoverflow.com/questions/14037404/java-read-large-text-file-with-70million-line-of-text

However, I found that most of these were simply discussions about either (1) different syntax which can be used
to achieve the same thing (e.g. loops, streams, Scanner);  (2) low-level details about classes in `java.io` and `java.nio` files, buffering, 
Reader vs InputStream, etc. In the end, I decided mostly to just roll my own solution.  Furthermore,
the most common recommendation was just to read file from the beginning until Nth line was found.  This
answer was unsatisfactory to me because as shown above, performance gets very poor for even medium sized
files.  I wanted response times in the range of 1-2 ms, not seconds. Some authors did make mention of
keeping track of newline characters in memory (what I have chosen to call "indexing") but these authors
also did not consider that one can run out of memory with millions of integers also.

I used [this](https://spring.io/guides/gs/spring-boot/) tutorial to refresh my memory on how to structure SpringBoot apps:


## What third party libraries or tools does the system use?

System makes very little use of 3rd party libraries, beyond the following:

* Java 1.8
* [Maven](https://maven.apache.org/) - Build and dependency management
* [Spring Boot](https://spring.io/projects/spring-boot) - Web layer
* [JUnit](https://junit.org/junit4/) - unit tests
* [Apache CLI](https://commons.apache.org/proper/commons-cli/) - Command line processing

## How long did you spend on this exercise? If you had unlimited more time to spend on this, how would you spend it and how would you prioritize each item?

I spent about 20 hours on this exercise spread out over 3 days.  Much of this time was spent on unit test automation.
If I had unlimited time to spend, I would do the following:

1. Try to find a single unified strategy which worked best, instead of 4 different and using a heuristic to choose the one to iuse.
2. Read some more scholarly articles to find if there is a best practice for demarcating strings within very large files.
3. If there is no unified strategy:  make the heuristic smarter based on average/min/max line length.
4. Multithread the indexing.  Current indexing time of ~60s for a 2GB file is unacceptable.  Would like to get this down to < 5 sec per 2 GB
5. "Productize" the test file generation and benchmarking code so that it can be easily run with one click from command line and results compared.
6. Look for low-level IO optimizations (buffering read/writes with larger buffers)
7. Scalability and concurrency test.

I would probably start with 4, 5, 6 since these are more tangible and I already have a good sense of what
needs to be done, and look for a "next generation" solution  in the background while maintaining the app.

## If you were to critique your code, what would you have to say about it?

Positives

1. Code accomplishes the task.
2. Appropriate framework chosen for build (Maven), web layer (SpringBoot), etc
3. Different strategies represented as different classes.
4. Author provided test coverage.
5. Author provided tools and utilities to make the project more usable (generate text files, benchmark, runtime perf stats)

Negatives

1. Code is more at the prototype level rather than a real API.  None of solutions is as sophisticated as I would like.
2. Code uses Spring but does not use Spring Configuration for the main classes / objects.  Singleton anti-pattern used.
3. The LineReader API uses a form of constructor injection which is the most awkward form of dependency injection.
4. Command line handling was probably overkill.

## Authors

* **Craig Milley** - *All work* - [Bitbucket Repo](https://bitbucket.org/craig_milley/lineserver)