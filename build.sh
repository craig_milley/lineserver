#!/bin/bash

if ! [ -x "$(command -v mvn)" ]; then
  echo 'Error: mvn is not installed.' >&2
  exit 1
fi

mvn clean install
if [[ "$?" -ne 0 ]] ; then
  echo 'could not install LineServer'; exit $rc
fi
