#!/bin/bash

display_usage() {
  echo
  echo "Usage: $0  [path_to_text_file]"
  echo
}

raise_error() {
  local error_message="$@"
  echo "${error_message}" 1>&2;
}

argument="$1"

if [[ -z $argument ]] ; then
  raise_error "Expected argument [path_to_text_file] to be present"
  display_usage
else
  java -jar target/LineServerApp-1.0-SNAPSHOT.jar --file "$@"
fi

