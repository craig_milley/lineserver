package com.craigmilley.lineserver.app;

import static org.junit.Assert.*;

import com.craigmilley.lineserver.reader.LineReaderTestBase;
import com.craigmilley.lineserver.reader.SimpleMemoryLineReader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test the controller logic.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LinesControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * Configure the application to read from the test file.
     * TODO:  workaround - bootstrap the environment with command line arguments.
     */
    @Before
    public void before() {
        LineServer.reader = new SimpleMemoryLineReader(LineReaderTestBase.SMALL_TEST_FILE);
    }

    /**
     * Test the happy path of requesting valid lines from the file.
     */
    @Test
    public void testGetValidLines() {
        assertEquals("Craig", getBody("1"));
        assertEquals("Milley", getBody("2"));
        assertEquals("", getBody("3"));
        assertEquals("Was", getBody("4"));
        assertEquals("Here", getBody("5"));
    }

    /**
     * Test that line numbers outside the bounds of the input file return HTTP 413.
     */
    @Test
    public void testGetInvalidLineIndexes() {
        final int EXPECTED_STATUS = 413;

        // Line numbers are 1-indexed, so 0 should return an error.
        ResponseEntity<String> entity = getEntity("0");
        assertEquals(EXPECTED_STATUS, entity.getStatusCode().value());

        // Test file only has 5 lines.
        entity = getEntity("6");
        assertEquals(EXPECTED_STATUS, entity.getStatusCode().value());

        entity = getEntity(Integer.toString(Integer.MAX_VALUE));
        assertEquals(EXPECTED_STATUS, entity.getStatusCode().value());

        entity = getEntity(Integer.toString(Integer.MIN_VALUE));
        assertEquals(EXPECTED_STATUS, entity.getStatusCode().value());
    }

    /**
     * Test that invalid line numbers - strings which are not even numbers, numbers > {@link Integer#MAX_VALUE}, or
     * numbers < {@link Integer#MIN_VALUE} return error code 400.
     */
    @Test
    public void testGetInvalidParams() {
        final int EXPECTED_STATUS = 400;

        // Not a number.
        ResponseEntity<String> entity = getEntity("abc");
        assertEquals(EXPECTED_STATUS, entity.getStatusCode().value());

        // Line number > Integer.MAX_VALUE cannot be parsed
        entity = getEntity(Long.toString(1L + Integer.MAX_VALUE));
        assertEquals(EXPECTED_STATUS, entity.getStatusCode().value());

        // Line number < Integer.MIN_VALUE cannot even be parsed
        entity = getEntity(Long.toString((long) Integer.MIN_VALUE - 1));
        assertEquals(EXPECTED_STATUS, entity.getStatusCode().value());
    }

    /**
     * Test retrieval of the statistics.
     */
    @Test
    public void testStats() {
        String stats = getStats();
        assertTrue(stats.contains("Number of requests processed:  0"));
        assertFalse(stats.contains("Avg. time per request:"));

        // Request a line number to update the stats.
        getBody("1");

        stats = getStats();
        assertTrue(stats.contains("Number of requests processed:  1"));
        assertTrue(stats.contains("Avg. time per request:"));
    }

    private String getBody(String lineNumber) {
        // TODO:  Determine why empty strings are being converted to null.  For now, just work around it.
        ResponseEntity<String> entity = getEntity(lineNumber);
        assertEquals(200, entity.getStatusCode().value());
        return entity.getBody() == null ? "" : entity.getBody();
    }

    private ResponseEntity<String> getEntity(String lineNumber) {
        return restTemplate.getForEntity("http://localhost:" + port + "/lines/" + lineNumber, String.class);
    }

    private String getStats() {
        ResponseEntity<String> entity = restTemplate.getForEntity("http://localhost:" + port + "/lines/stats", String.class);
        assertEquals(200, entity.getStatusCode().value());
        assertTrue(entity.getHeaders().get(HttpHeaders.CONTENT_TYPE).get(0).contains("text/plain"));
        return entity.getBody();
    }

}
