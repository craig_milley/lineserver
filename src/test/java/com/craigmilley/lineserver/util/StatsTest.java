package com.craigmilley.lineserver.util;

import com.craigmilley.lineserver.util.Stats;
import org.junit.Test;

import static org.junit.Assert.*;

public class StatsTest {
    @Test
    public void testAll() {
        Stats stats = new Stats();
        validate(stats, 0l, null, null, null,
                null, null, null, null);
        assertTrue(stats.toFancyString().contains("Number of requests processed:  0"));

        // Update 1
        stats.update(1000, 50);
        validate(stats, 1l, 1000.0, 50.0, .05,
                1000l, 1000l, 50l, 50l);
        assertTrue(stats.toFancyString().contains("Number of requests processed:  1"));

        // Update 2
        stats.update(2000, 100);
        validate(stats, 2l, 1500.0, 75.0, .05,
                2000l, 1000l, 100l, 50l);
        assertTrue(stats.toFancyString().contains("Number of requests processed:  2"));
    }

    private void validate(Stats stats,
                          long expectedNumRequests,
                          Double expectedAvgLineNum,
                          Double expectedAvgTimeMillis,
                          Double expectedAvgTimePerLineMillis,
                          Long expectedLineNumMax,
                          Long expectedLineNumMin,
                          Long expectedMaxTimeMillis,
                          Long expectedMinTimeMillis) {

        assertEquals(expectedNumRequests, stats.getNumRequests());
        assertEquals(expectedMinTimeMillis, stats.getMinTimeMillis());
        assertEquals(expectedMaxTimeMillis, stats.getMaxTimeMillis());
        assertEquals(expectedAvgTimeMillis, stats.getAvgTimeMillis());
        assertEquals(expectedAvgLineNum, stats.getAvgLineNum());
        assertEquals(expectedLineNumMin, stats.getLineNumMin());
        assertEquals(expectedLineNumMax, stats.getLineNumMax());
        assertEquals(expectedAvgTimePerLineMillis, stats.getAvgTimePerLineMillis());
    }
}
