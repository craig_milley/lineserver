package com.craigmilley.lineserver.reader;

import org.junit.After;

import java.io.File;
import java.util.Arrays;

public class LineReaderWithFileIndexTest extends LineReaderTestBase<LineReaderWithFileIndex> {

    @Override
    protected LineReaderWithFileIndex getLineReader(File f) {
        return new LineReaderWithFileIndex(f);
    }

    @After
    public void after() {
        deleteIndexFiles();
    }

    private void deleteIndexFiles() {
        Arrays.stream(TEST_DIR.listFiles((dir, name) -> name.endsWith(".index"))).forEach(f -> f.delete());
    }
}
