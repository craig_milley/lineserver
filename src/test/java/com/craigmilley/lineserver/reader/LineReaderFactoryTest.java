package com.craigmilley.lineserver.reader;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * White box test suite for LineReaderFactory.  Validates that the expected implementation class is returned for a given
 * text file. Currently this computation is done only based on file size so suite validates various sizes.
 */
public class LineReaderFactoryTest {

    private static final long KB = 1024;
    private static final long MB = 1024 * KB;
    private static final long GB = 1024 * MB;

    private LineReaderFactory instance = new LineReaderFactory();

    @Test
    public void test0ByteFile() {
        assertReaderClass(0, LineReaderWithFileIndex.class);
    }

    @Test
    public void testFileLessThan20MB() {
        assertReaderClass(20 * MB - 1, SimpleMemoryLineReader.class);
        assertReaderClass(1, SimpleMemoryLineReader.class);
    }

    @Test
    public void testFileLessThan100MB() {
        assertReaderClass(20 * MB, SimpleFileLineReader.class);
        assertReaderClass(100 * MB - 1, SimpleFileLineReader.class);
    }

    @Test
    public void testFileLessThan1GB() {
        assertReaderClass(100 * MB, LineReaderWithMemoryIndex.class);
        assertReaderClass(1 * GB - 1, LineReaderWithMemoryIndex.class);

    }

    @Test
    public void testFileGreaterThan1GB() {
        assertReaderClass(1 * GB, LineReaderWithFileIndex.class);

    }

    private void assertReaderClass(long bytes, Class expectedClass) {
        LineReader reader = instance.getLineReaderForFileLength(LineReaderTestBase.SMALL_TEST_FILE, bytes);
        assertEquals(expectedClass, reader.getClass());
    }
}
