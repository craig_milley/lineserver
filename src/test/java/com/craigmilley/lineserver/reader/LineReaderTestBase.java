package com.craigmilley.lineserver.reader;

import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;


/**
 * Base class for LineReader related tests.  Takes an implementation of LineReader interface and runs a standard set of
 * simple unit tests against it, using a small txt file in the test fixture for data.
 */
@Ignore
public abstract class LineReaderTestBase<T extends LineReader> {

    public static final File TEST_DIR = new File("src/test/data");
    public static final File SMALL_TEST_FILE = new File(TEST_DIR, "SmallTestFile.txt");
    public static final File MEDIUM_TEST_FILE = new File(TEST_DIR, "MediumTestFile.txt");
    public static final File EMPTY_TEST_FILE = new File(TEST_DIR, "EmptyFile.txt");
    public static final File ALL_EMPTY_LINES_TEST_FILE = new File(TEST_DIR, "AllEmptyLines.txt");


    protected LineReaderTestBase() {
    }

    protected abstract T getLineReader(File f);

    /**
     * Happy path test case.  Retrieve all lines in the file one by one.
     */
    @Test
    public void testSmallFile_GetValidLines() throws IOException {
        LineReader instance = getLineReader(SMALL_TEST_FILE);
        assertEquals("Craig", instance.getLine(1));
        assertEquals("Milley", instance.getLine(2));
        assertEquals("", instance.getLine(3));
        assertEquals("Was", instance.getLine(4));
        assertEquals("Here", instance.getLine(5));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSmallFile_GetLineNumberBelowMin() throws IOException {
        LineReader instance = getLineReader(SMALL_TEST_FILE);
        instance.getLine(0);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSmallFile_GetNegativeLineNumber() throws IOException {
        LineReader instance = getLineReader(SMALL_TEST_FILE);
        instance.getLine(-1);
    }


    @Test(expected = IndexOutOfBoundsException.class)
    public void testSmallFile_GetLineNumberAboveMax() throws IOException {
        LineReader instance = getLineReader(SMALL_TEST_FILE);
        instance.getLine(6);
    }

    @Test
    public void testMediumFile_GetValidLines() throws IOException {
        LineReader instance = getLineReader(MEDIUM_TEST_FILE);
        int[] testLines = {1, 999, 1000, 1001, 1999, 2000, 2001, 999_999, 1_000_000};
        for (int i = 0; i < testLines.length; i++) {
            int n = testLines[i];
            assertEquals("This is line #" + n, instance.getLine(n));
        }

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testMediumFile_GetLineNumberBelowMin() throws IOException {
        LineReader instance = getLineReader(MEDIUM_TEST_FILE);
        instance.getLine(0);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testMediumFile_GetNegativeLineNumber() throws IOException {
        LineReader instance = getLineReader(MEDIUM_TEST_FILE);
        instance.getLine(-1);
    }


    @Test(expected = IndexOutOfBoundsException.class)
    public void testMediumFile_GetLineNumberAboveMax() throws IOException {
        LineReader instance = getLineReader(MEDIUM_TEST_FILE);
        instance.getLine(1_000_001);
    }

    /**
     * Validate that an empty file contains no lines.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testEmptyFile() throws IOException {
        LineReader instance = getLineReader(EMPTY_TEST_FILE);
        instance.getLine(1);
    }

    /**
     * Validate that a file which contains only blank lines (i.e. file contains only newline chars) returns lines as
     * expected.
     */
    @Test
    public void testAllBlankFile_GetValidLines() throws IOException {
        LineReader instance = getLineReader(ALL_EMPTY_LINES_TEST_FILE);
        for (int i = 1; i <= 7; i++) {
            assertEquals("", instance.getLine(1));
        }
    }

    /**
     * Validate that a file which contains only blank lines (i.e. file contains only newline chars) throws an exception
     * if a line about the max line number is passed.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testAllBlankFile_GetLineNumberAboveMax() throws IOException {
        LineReader instance = getLineReader(ALL_EMPTY_LINES_TEST_FILE);
        instance.getLine(8);
    }
}
