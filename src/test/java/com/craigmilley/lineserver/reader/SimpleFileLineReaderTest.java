package com.craigmilley.lineserver.reader;

import java.io.File;

public class SimpleFileLineReaderTest extends LineReaderTestBase<SimpleFileLineReader> {

    @Override
    protected SimpleFileLineReader getLineReader(File f) {
        return new SimpleFileLineReader(f);
    }
}
