package com.craigmilley.lineserver.reader;

import java.io.File;

public class LineReaderWithSignpostsTest extends LineReaderTestBase<LineReaderWithSignposts>  {

    @Override
    protected LineReaderWithSignposts getLineReader(File f) {
        return new LineReaderWithSignposts(f);
    }
}
