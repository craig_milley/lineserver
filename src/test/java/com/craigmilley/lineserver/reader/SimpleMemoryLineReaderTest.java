package com.craigmilley.lineserver.reader;

import java.io.File;

public class SimpleMemoryLineReaderTest extends LineReaderTestBase<SimpleMemoryLineReader> {
    @Override
    protected SimpleMemoryLineReader getLineReader(File f) {
        return new SimpleMemoryLineReader(f);
    }
}
