package com.craigmilley.lineserver.reader;

import java.io.File;

public class LineReaderWithMemoryIndexTest extends LineReaderTestBase<LineReaderWithMemoryIndex> {

    @Override
    protected LineReaderWithMemoryIndex getLineReader(File f) {
        return new LineReaderWithMemoryIndex(f);
    }
}
