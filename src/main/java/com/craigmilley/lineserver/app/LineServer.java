package com.craigmilley.lineserver.app;

import com.craigmilley.lineserver.reader.LineReader;
import com.craigmilley.lineserver.reader.LineReaderFactory;
import org.apache.commons.cli.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

/**
 * Entry point for the Spring Boot web application.
 */
@SpringBootApplication
public class LineServer {

    /**
     * The single instance of the LineReader that the server uses to process requests.
     */
    public static LineReader reader;

    /**
     * Process the command line and start the application.  The command line must be of the form <code>-f [PATH]</code>
     * or else the server will not start. If an existing text file is passed, create a singleton instance of the
     * LineReader which will be used for extracting text from the text file.
     *
     * @param args The command line args.  Must be of the form <code>-f [PATH]</code>.
     */
    public static void main(String[] args) {
        // Parse the command line and validate for required arguments.
        CommandLine cmd = getParsedCommandLine(args);

        // Get the input file and proactively check for it's existence.
        String path = cmd.getOptionValue("file");
        File file = new File(path);
        if (!file.exists()) {
            throw new IllegalStateException(String.format("Input file [%s] does not exist.", file.getAbsolutePath()));
        }

        // Create the reader from the specified input file.
        LineReaderFactory factory = new LineReaderFactory();
        reader = factory.getLineReader(file);

        // Start the server.  Let SpringBoot framework do most of the work.
        SpringApplication.run(LineServer.class, args);
    }


    private static CommandLine getParsedCommandLine(String[] args) {
        // Assemble object representing the command line options.
        Options options = new Options();
        options.addOption(Option.builder().longOpt("file").argName("file").hasArgs().required().desc("Input file path.").build());

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (Exception ex) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(LineServer.class.getSimpleName(), options);
            System.exit(0);
        }
        return cmd;
    }
}
