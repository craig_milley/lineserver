package com.craigmilley.lineserver.app;

import com.craigmilley.lineserver.util.Stats;
import org.springframework.http.HttpStatus;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

/**
 * Rest LinesController for the LineServer.
 */
@RestController
public class LinesController {

    /**
     * Statistics object maintaining basic performance statistics about the line server.
     */
    private Stats stats = new Stats();

    /**
     * Process a request to the root URL by returning a friendly message.
     * @return A friendly message to display.
     */
    @GetMapping("/")
    public String root() {
        return "Welcome to Line Server.";
    }

    /**
     * Process a request of the form
     * <code>
     * GET /lines/<line_number>
     * </code>
     * Return the specified line of the file if found or throws an exception otherwise.
     * <p/>
     * Note that this controller maintains some basic performance about how long it takes to retrieve the lines from
     * the text file during processing of this call.
     *
     * @param strLineNum The line number to retrieve from the file.  NOTE:  Line numbers are one-indexed.
     * @return The requested line from the text file.
     * @throws IOException             Exception if there is an error reading from the text file.
     * @throws ResponseStatusException HTTP Error 413 if line_number is not a valid line number.  400 if line_number is
     *                                 not even a number.
     */
    @GetMapping("/lines/{line_number}")
    public String getLine(@PathVariable("line_number") String strLineNum) throws IOException, ResponseStatusException {
        Integer lineNum = null;
        try {
            // Start a timer for simple performance monitoring.
            StopWatch watch = new StopWatch();
            watch.start();

            // Parse the request.
            lineNum = Integer.parseInt(strLineNum);

            // Call the singleton LineReader instance to get the requested line.
            String line = LineServer.reader.getLine(lineNum);

            // Record the time it took to retrieve the line.
            watch.stop();
            stats.update(lineNum, watch.getLastTaskTimeMillis());

            // return the line
            return line;

        } catch (IndexOutOfBoundsException ex) {
            // return HTTP 413 if index is out of bounds.
            throw new ResponseStatusException(HttpStatus.PAYLOAD_TOO_LARGE,
                    String.format("Line number [%d] is out of bounds.", lineNum));
        } catch (NumberFormatException ex) {
            // return HTTP 400 if parameter is not a number
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Line number [%s] is invalid.", strLineNum));
        }
    }

    /**
     * Get a string representation of the performance statistics.
     *
     * @return a stringified representation of the Statistics object.
     */
    @GetMapping(value="/lines/stats", produces="text/plain")
    @ResponseBody
    public String getStats() {
        return stats.toFancyString();
    }

}
