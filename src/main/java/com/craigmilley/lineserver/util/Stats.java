package com.craigmilley.lineserver.util;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Class for storing basic performance metrics for the Line Server.  Used for getting a rough idea of performance.
 * Currently the only thing that can be done with this object is reading the statistics via {@link #toString()}.
 */
public class Stats {

    private long numRequests = 0;
    private Double avgTimeMillis = null;
    private Long minTimeMillis = null;
    private Long maxTimeMillis = null;
    private Double avgLineNum = null;
    private Long lineNumMin = null;
    private Long lineNumMax = null;
    private Double avgTimePerLineMillis = null;

    /**
     * Update the statistics with the timings for a single call to the Line Server. Note:  This method is synchronized
     * and therefore may be a bottleneck in a high traffic situation, but synchronization is needed, since update
     * mutates all the statistics.
     *
     * @param lineNum the requested line number.
     * @param ms      time it took to process the single request to the line server.
     */
    public synchronized void update(long lineNum, long ms) {
        numRequests++;
        avgTimeMillis = updateAvg(avgTimeMillis, ms, numRequests);
        minTimeMillis = updateMin(minTimeMillis, ms);
        maxTimeMillis = updateMax(maxTimeMillis, ms);
        avgTimePerLineMillis = updateAvg(avgTimePerLineMillis, ((double) ms) / lineNum, numRequests);
        lineNumMin = updateMin(lineNumMin, lineNum);
        lineNumMax = updateMax(lineNumMax, lineNum);
        avgLineNum = updateAvg(avgLineNum, lineNum, numRequests);
    }

    public long getNumRequests() {
        return numRequests;
    }

    public Double getAvgTimeMillis() {
        return avgTimeMillis;
    }

    public Long getMinTimeMillis() {
        return minTimeMillis;
    }

    public Long getMaxTimeMillis() {
        return maxTimeMillis;
    }

    public Double getAvgLineNum() {
        return avgLineNum;
    }

    public Long getLineNumMin() {
        return lineNumMin;
    }

    public Long getLineNumMax() {
        return lineNumMax;
    }

    public Double getAvgTimePerLineMillis() {
        return avgTimePerLineMillis;
    }

    /**
     * Return a fancy stringified representation of the performance statistics.
     *
     * @return
     */
    public String toFancyString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Number of requests processed:  %,d\n", numRequests));
        if (numRequests > 0) {
            sb.append(String.format("Avg. time per request:         %.2f ms\n", avgTimeMillis));
            sb.append(String.format("Min request time:              %,d ms\n", minTimeMillis));
            sb.append(String.format("Max request time:              %,d ms\n", maxTimeMillis));
            sb.append(String.format("Total time:                    %,.0f ms\n", avgTimeMillis * numRequests));
            sb.append(String.format("Avg. line number requested:    %,.0f\n", avgLineNum));
            sb.append(String.format("Min line number:               %,d\n", lineNumMin));
            sb.append(String.format("Max line number:               %,d\n", lineNumMax));
            sb.append(String.format("Avg. time spent per line:      %.2f μs\n", avgTimePerLineMillis * 1000));
        }
        return sb.toString();
    }

    public String toString() {
        return new ReflectionToStringBuilder(this).toString();
    }

    private Double updateAvg(Double oldAvg, Long newValue, Long newDenominator) {
        oldAvg = oldAvg == null ? 0 : oldAvg;
        return ((newDenominator - 1) * oldAvg + newValue) / newDenominator;
    }

    private Double updateAvg(Double oldAvg, Double newValue, Long newDenominator) {
        oldAvg = oldAvg == null ? 0 : oldAvg;
        return ((newDenominator - 1) * oldAvg + newValue) / newDenominator;
    }

    private Long updateMin(Long oldMin, Long newValue) {
        return oldMin == null ? newValue : Math.min(newValue, oldMin);
    }

    private Long updateMax(Long oldMax, Long newValue) {
        return oldMax == null ? newValue : Math.max(newValue, oldMax);
    }
}
