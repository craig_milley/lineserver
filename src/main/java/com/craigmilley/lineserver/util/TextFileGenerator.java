package com.craigmilley.lineserver.util;

import java.io.*;
import java.util.function.Function;

/**
 * Generate some text files for benchmarking.
 */
public class TextFileGenerator {

    public static final String MEDIUM_TEST_FILE = "MediumTestFile.txt";
    public static final String MEDIUM_TEST_FILE_WITH_LONGER_LINES = "MediumTestFileWithLongerLines.txt";
    public static final String LARGE_TEST_FILE = "LargeTestFile.txt";
    public static final String LARGE_TEST_FILE_WITH_LONGER_LINES = "LargeTestFileWithLongerLines.txt";


    /**
     * Generate test files for benchmarking:
     *
     * <ul>
     *     <li>test-data/MediumTestFileWithLongerLines.txt (549 MB)</li>
     *     <li>test-data/LargeTestFile.txt (2.29 GB)</li>
     *     <li>test-data/LargeTestFileWithLongerLines.txt (2.2 GB)</li>
     * </ul>
     *
     * Warning:  This creates over 5 GB of test data.  Only run if you have enough space!
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // directory is just a subdir of working directory.
        File dir = new File("./test-data");
        dir.mkdirs();

        // Generate the 3 files
        System.out.println(String.format("Generating test files in [%s]", dir.getAbsolutePath()));

        createMediumFile(dir);
        createLargeFile(dir);
        createMediumFileWithLongerLines(dir);
        createLargeFileWithLongerLines(dir);

        System.out.println(String.format("Done generating test files in [%s]", dir.getAbsolutePath()));
    }

    /**
     * Utility to generate text files with a given pattern per line.
     *
     * @param file
     * @param numLines
     * @param lineGenerator
     * @return
     * @throws IOException
     */
    public static File generate(File file, int numLines, Function<Integer, String> lineGenerator) throws IOException {
        // Do not overwrite existing file.
        if (file.exists()) {
            System.out.println(String.format("File [%s] already exists.  Not overwriting.",
                    file.getAbsolutePath()));
            return file;
        }

        try (Writer writer = new BufferedWriter(new FileWriter(file))) {
            for (int i = 1; i <= numLines; i++) {
                writer.write(lineGenerator.apply(i) + "\n");
            }
        }
        System.out.println(String.format("Wrote file [%s] with [%,d] line and [%,d] bytes.",
                file.getAbsolutePath(), numLines, file.length()));

        return file;
    }

    public static File createMediumFile(File dir) throws IOException {
        return generate(new File(dir, MEDIUM_TEST_FILE),
                1_000_000, n -> "This is line #" + n);
    }

    /**
     * Create a file with 100_000_000 lines, each of the form "This is line #N". Resulting file is 2.3 GB.
     * <p/>
     * Note:  This is an example where building a file-based index containing the positions of all newline characters in
     * the file should be very inefficient since the line numbers are almost as long as the lines themselves.
     *
     * @param dir
     * @throws IOException
     */
    public static File createLargeFile(File dir) throws IOException {
        return generate(new File(dir, LARGE_TEST_FILE),
                100_000_000, n -> "This is line #" + n);
    }

    /**
     * Create a file with 4_000_000 lines, each of the form: "N: [540 letters]".
     * <p/>
     * Note:  This is an example where building a file-based  index containing the positions of all newline characters
     * in the file should require less space.
     *
     * @param dir
     * @throws IOException
     */
    public static File createLargeFileWithLongerLines(File dir) throws IOException {
        return generate(new File(dir, LARGE_TEST_FILE_WITH_LONGER_LINES),
                4_000_000, n -> createLongerLine(n));
    }

    /**
     * Create a file with 1_000_000 lines, each of the form: "N: [540 letters]".
     *
     * @param dir
     * @throws IOException
     */
    public static File createMediumFileWithLongerLines(File dir) throws IOException {
        return generate(new File(dir, MEDIUM_TEST_FILE_WITH_LONGER_LINES),
                1_000_000, n -> createLongerLine(n));
    }

    private static String createLongerLine(Integer n) {
        return n + ": " + repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZ ", 20);
    }

    // method already exists in JDK 11....
    private static String repeat(String s, int times) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < times; i++) {
            builder.append(s);
        }
        return builder.toString();
    }
}
