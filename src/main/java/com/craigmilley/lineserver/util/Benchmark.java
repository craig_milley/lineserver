package com.craigmilley.lineserver.util;

import com.craigmilley.lineserver.reader.LineReader;
import org.apache.commons.cli.*;
import org.springframework.util.StopWatch;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Random;

/**
 * Quick and dirty benchmarking class which makes a specified number of random requests for lines in a file using a
 * given LineReader implementation.  Results are stored in a {@link Stats} object.
 */
public class Benchmark {

    private final File file;
    private final String readerClass;
    private final long numTrials;
    private final int maxLineNumber;
    private final Stats stats = new Stats();
    private final Random random;

    public Benchmark(File file, String readerClass, long numTrials, int maxLineNumber) {
        this.file = file;
        this.readerClass = readerClass;
        this.numTrials = numTrials;
        this.maxLineNumber = maxLineNumber;
        this.random = new Random(System.currentTimeMillis());
    }

    /**
     * Run a benchmarking test based on command line args.  Write results to standard out.  Following command line
     * arguments are all required:
     *
     * <ul>
     * <li>--file:  path to the input text file</li>
     * <li>--reader: name of the implementation class of the LineReader</li>
     * <li>--trials:  the number of requests to make during the test</li>
     * <li>--lines:  the number of lines in the input file</li>
     * </ul>
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        CommandLine cmd = getParsedCommandLine(args);

        // Get the command line args.
        final String path = cmd.getOptionValue("file");
        final File f = new File(path);
        if (!f.exists()) {
            throw new IllegalStateException(String.format("Input file [%s] does not exist.", f.getAbsolutePath()));
        }

        final String readerClass = cmd.getOptionValue("reader");
        final int trials = Integer.parseInt(cmd.getOptionValue("trials"));
        final int lines = Integer.parseInt(cmd.getOptionValue("lines"));

        // Construct the benchmark object itself
        Benchmark benchmark = new Benchmark(f, readerClass, trials, lines);


        // Run the benchmark
        benchmark.run();
    }

    private static CommandLine getParsedCommandLine(String[] args) {
        Options options = new Options();
        options.addOption(Option.builder().longOpt("file").argName("file").hasArgs().required().desc("Input file path.").build());
        options.addOption(Option.builder().longOpt("reader").argName("class").hasArgs().required().desc("LineReader impl class.").build());
        options.addOption(Option.builder().longOpt("trials").argName("n").hasArgs().required().desc("The number of line requests.").build());
        options.addOption(Option.builder().longOpt("lines").argName("n").hasArgs().required().desc("Number of lines in file.").build());

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (Exception ex) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(Benchmark.class.getSimpleName(), options);
            System.exit(0);
        }
        return cmd;
    }

    /**
     * Run the benchmarking test.  Write the results to System.out.  Results of the test will be available by calling
     * {@link #getStats()} after completion.
     *
     * @throws Exception
     */
    public synchronized void run() throws Exception {

        System.out.println("Benchmark invoked.");
        System.out.println("------------------------------------------------------------------------------------");
        System.out.println(String.format("Input file: [%s]", file.getAbsolutePath()));
        System.out.println(String.format("LineReader class: [%s]", readerClass));
        System.out.println(String.format("Num lines to retrieve: [%,d]", numTrials));
        System.out.println(String.format("Lines in file: [%,d]", maxLineNumber));
        System.out.println("------------------------------------------------------------------------------------");

        Class clazz = Class.forName(readerClass);
        Constructor con = clazz.getConstructor(File.class);

        // Time the creation of the LineReader because it might do something heavy in the constructor.
        StopWatch watch = new StopWatch();
        watch.start();
        System.out.println(String.format("Constructing instance of %s.....", readerClass));

        // Construct the instance.  This might take time.
        LineReader reader = (LineReader) con.newInstance(file);

        // Stop the timer and emit the results.
        watch.stop();
        System.out.println(String.format("Constructed instance of %s in %dms", readerClass, watch.getLastTaskTimeMillis()));


        // Restart the timer
        System.out.println("Starting test.....");
        watch.start();

        try {
            // implement a super cheesy progress bar
            int pctDone = 0;
            int lastPctDone = 0;

            for (int i = 0; i < numTrials; i++) {
                // Start a watch to time the line retrieval.
                StopWatch innerWatch = new StopWatch();
                innerWatch.start();

                // Get the requested line.
                int lineNum = random.nextInt(maxLineNumber) + 1;
                reader.getLine(lineNum);

                // Stop the watch and update the statistics.
                innerWatch.stop();
                stats.update(lineNum, innerWatch.getLastTaskTimeMillis());

                // update the super cheesy progress bar
                pctDone = (int) (100 * i / numTrials);
                if (pctDone > 0 && pctDone != lastPctDone) {
                    for (int tick = lastPctDone + 1; tick <= pctDone; tick++) {
                        System.out.print(".");
                    }
                    lastPctDone = pctDone;
                }
            }
            System.out.println();

            // Stop the timer and emit the results.
            watch.stop();
            System.out.println(String.format("Ran test in %dms", watch.getLastTaskTimeMillis()));
            System.out.println("Test results:");
            System.out.println();
            System.out.println(getStats().toFancyString());

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public Stats getStats() {
        return stats;
    }

}
