package com.craigmilley.lineserver.reader;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * File reader which accesses the file on the file system each request, starts reading lines from the beginning of the
 * file using {@link java.nio.file.Files#lines(Path)} API and stopping when the desired line is reached.
 * <p/>
 * Note:  This method is not suitable for very large files because it always reads the file from the beginning until it
 * reaches the requested line.
 */
public class SimpleFileLineReader extends LineReader {

    public SimpleFileLineReader(File f) {
        super(f);
    }

    @Override
    public String getLine(final int lineNumber) throws IOException {
        validateLineNumber(lineNumber);
        return Files.lines(textFile.toPath())
                .skip(lineNumber - 1)
                .findFirst()
                .orElseThrow(ArrayIndexOutOfBoundsException::new);
    }
}
