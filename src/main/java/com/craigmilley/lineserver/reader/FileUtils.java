package com.craigmilley.lineserver.reader;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Helper class with some generally-useful utility methods.
 */
public class FileUtils {

    // Private constructor for utility only class.
    private FileUtils() {

    }

    /**
     * Extract a subset of a file into a String.  This method internally uses  {@link
     * java.io.RandomAccessFile#seek(long)} to position the curse for reading.
     *
     * @param file  The file to read from.
     * @param start the star position of the desired string
     * @param end   the end position of the desired string, exclusive
     * @return The extracted string.
     * @throws IOException if an general IO error occurs while reading.
     *
     */
    public static String getFromFile(File file, long start, long end) throws IOException {
        try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            long count = end - start;

            // Init a buffer to hold our line.
            StringBuffer line = new StringBuffer();

            // Seek to the appropriate spot in the file.
            raf.seek(start);

            // Read in the correct number of bytes.
            // It is safe to interpret each byte as character since file is presumed ASCII encoded.
            // TODO:  Buffer the reading.
            for (int i = 0; i < count; i++) {
                line.append((char) raf.readUnsignedByte());
            }

            return line.toString();
        }
    }
}
