package com.craigmilley.lineserver.reader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * Trivial implementation of LineReader interface.  Reads the entire file and parses it into lines at construction time.
 * All lines are stored in memory.
 * <p/>
 * NOTE:  This class is only suitable for very small text files.  Otherwise out-of-memory exceptions can occur.
 */
public class SimpleMemoryLineReader extends LineReader {

    private final List<String> lines = new ArrayList<>();

    public SimpleMemoryLineReader(File textFile) {
        super(textFile);

        try {
            Files.lines(textFile.toPath()).forEach(lines::add);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getLine(int lineNumber) {
        validateLineNumber(lineNumber);
        return lines.get(lineNumber - 1);
    }
}
