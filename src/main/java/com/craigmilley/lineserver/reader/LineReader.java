package com.craigmilley.lineserver.reader;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * Interface for all readers which can retrieve lines from a single text file.  Files are presumed to be ASCII and
 * delimited by "\n" and not "\r\n".
 */
public abstract class LineReader {

    /**
     * The text file which this reader reads from.
     */
    protected final File textFile;

    /**
     * Construct a reader from the passed text file.
     *
     * @param textFile The text file to reead lines from.  Must not be null and must ecxist.
     */
    protected LineReader(File textFile) {
        this.textFile = Objects.requireNonNull(textFile);
        if (!textFile.exists()) {
            throw new IllegalArgumentException(String.format("File [%s] does not exist ", textFile));
        }
    }

    /**
     * Get a line from the text file.
     *
     * @param lineNumber The line number to retrieve, 1-indexed so that that 1 is the first line in the file, not 0.
     *                   Note also that because this argument is an int, the maximum number of lines the text file may
     *                   contain is:  {@link Integer#MAX_VALUE}.
     * @return The requested line.
     * @throws ArrayIndexOutOfBoundsException if the requested index is out of bounds.
     * @throws IOException                    if a general error occurs attempting to read from the file.
     */
    public abstract String getLine(int lineNumber) throws ArrayIndexOutOfBoundsException, IOException;

    /**
     * Helper method for subclasses to standardize line number validation.
     *
     * @param lineNumber
     */
    protected void validateLineNumber(int lineNumber) {
        Integer lineCount = getLineCount();
        // line count may be null
        if (lineNumber < 1 || (lineCount != null && lineNumber > lineCount)) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    /**
     * Return the number of lines in the file.  This might return null for implementations which do no pre-read the
     * entire file.
     *
     * @return
     */
    public Integer getLineCount() {
        return null;
    }
}
