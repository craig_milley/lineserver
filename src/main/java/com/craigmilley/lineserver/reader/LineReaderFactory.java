package com.craigmilley.lineserver.reader;

import java.io.File;

/**
 * Factory class for returning an appropriate LineReader implementation for a given text file.
 */
public class LineReaderFactory {

    private static final long KB = 1024;
    private static final long MB = 1024 * KB;
    private static final long GB = 1024 * MB;

    /**
     * Get an appropriate LineReader implementation based on text file we want to extract text from.  Currently this
     * decision is based purely on file size according to the following rules:
     *
     * <ul>
     * <li>(0, 20MB):  {@link SimpleMemoryLineReader}.  Simply stores all lines in memory.</li>
     * <li>[20MB, 100MB):  {@link SimpleFileLineReader}.  Simply read directly from the file at access time starting at
     * the beginning until requested line is found.</li>
     * <li>[100MB, 1G):  {@link SimpleMemoryLineReader}.  Keep list (i.e. "index") of newline positions in memory.  Use
     * this at request-time to determine the position of the line to extract.</li>
     * <li>[1GB, ]:  {@link LineReaderWithFileIndex}.  Keep list of newline positions on disk in an "index" file. Use
     * this at request-time to determine the position of the line to extract.</li>
     * </ul>
     *
     * See individual implementation classes for explanation of tradeoffs.
     * <p/>
     * Note:  Using file size as the only heuristic is merely a guess and needs to be refined.  Line length is also
     * a factor.  For pathological text files, the returned Reader implementation might be totally inappropriate. For
     * example, a LineReaderWithFileIndex would be totally inappropriate for a file containing all empty lines, since
     * the size of the index would far exceed size of the original file.
     *
     * @param f
     * @return
     */
    public LineReader getLineReader(File f) {
        return getLineReaderForFileLength(f, f.length());
    }

    // Artificial method.  Workaround for Mockito.  Cannot mock java.io.File for unit tests.
    public LineReader getLineReaderForFileLength(File f, long len) {
        if (len > 0l) {
            if (len >= GB) {
                // For  text files >= 1GB keep an index on disk of the newline locations.
                return new LineReaderWithFileIndex(f);
            } else if (len >= 100 * MB) {
                // For  text files [100MB, 1GB) keep an index in memory of the newline locations.
                return new LineReaderWithMemoryIndex(f);
            } else if (len >= 20 * MB) {
                // For text files [20MB, 100MB) just read directly from the file using streams API
                return new SimpleFileLineReader(f);
            } else {
                // For text files (0MB, 20MB) just keep all the lines in memory.
                return new SimpleMemoryLineReader(f);
            }
        } else {
            // Cannot determine length so return the safest choice.
            return new LineReaderWithFileIndex(f);
        }
    }
}
