package com.craigmilley.lineserver.reader;

import org.bitbucket.kienerj.io.OptimizedRandomAccessFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


/**
 * LineReader which reads the entire file at construction time and builds an in-memory table of the position of every
 * Nth newline characters in the file.  We call these positions "signposts".  At request time, uses {@link
 * java.io.RandomAccessFile} API to extract the requested line from the file.  The signposts are used so skip to a line
 * in the file which is close to the requested file. We might have to read up to N-1 lines from the file in order to
 * find the requested line.
 */
public class LineReaderWithSignposts extends LineReader {

    /**
     * The number of lines to read before recording a position of a newline character.  This means we record the
     * position of every Nth newline character where N == SIGNPOST_LINES.
     */
    public static final int SIGNPOST_LINES = 1000;

    /**
     * The positions of every Nth newline character.
     */
    private List<Long> signposts = new ArrayList<>();

    /**
     * Count of the lines in the file.  Set to the correct number in the constructor.
     */
    private int lineCount = 0;


    public LineReaderWithSignposts(File textFile) {
        super(textFile);
        initSignposts();
    }

    @Override
    public Integer getLineCount() {
        return lineCount;
    }

    @Override
    public String getLine(final int lineNumber) throws IOException {
        validateLineNumber(lineNumber);

        final int lineIndex = lineNumber - 1;

        // Determine starting position within the file.
        long start;
        if (lineNumber <= SIGNPOST_LINES) {
            start = 0; // start at beginning of file.
        } else {
            start = signposts.get((lineIndex / SIGNPOST_LINES) - 1) + 1; // start at the signpost position + 1
        }

        // Determine number of lines that need to be skipped starting at starting position to retrieve the desired line.
        int skipLines = lineIndex % SIGNPOST_LINES;

        // Open up file for random access
        OptimizedRandomAccessFile raf = null;
        try  {

            raf = new OptimizedRandomAccessFile(textFile, "r");

            // Seek to the appropriate spot in the file.
            raf.seek(start);

            // Skip the calculated number of lines;
            for (int i = 0; i < skipLines; i++) {
                raf.readLine();
            }

            // Return the requested line.
            return raf.readLine();
        }
        finally {
            if (raf != null) {
                raf.close();
            }
        }
    }

    /**
     * Read the entire file.  Record the position of every Nth newline character.
     */
    private void initSignposts() {
        long pos = 0;
        long lastNewlinePos = 0;
        try (InputStream in = new BufferedInputStream(new FileInputStream(textFile))) {
            int c;
            while ((c = in.read()) >= 0) {
                if (c == '\n') {
                    lastNewlinePos = pos;
                    if ((++lineCount % SIGNPOST_LINES) == 0) {
                        signposts.add(pos);
                    }

                }
                pos++;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Treat EOF like a newline if it is not preceded immediately by a newline
        if (pos > 0 && pos != lastNewlinePos + 1) {
            lineCount++;
        }
    }
}
