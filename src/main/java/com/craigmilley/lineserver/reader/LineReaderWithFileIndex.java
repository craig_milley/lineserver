package com.craigmilley.lineserver.reader;

import java.io.*;

/**
 * LineReader which reads the entire file at construction time and builds an index on disk containing the positions of
 * all newline characters in the file.  At request time, it uses this index to determine the position of the requested
 * line and then uses the {@link java.io.RandomAccessFile#seek(long)} API to extract the requested line from the file.
 * <p/>
 * NOTE:  This implementation will work for even very large text files but can be inefficient in terms of disk space
 * used.  The index which is built uses a fixed width of {@link #INDEX_ENTRY_FIXED_WIDTH} for storing the position of
 * the newline characters.  This fixed width allows for fast seeking, but also involves a lot of wasted space. The worst
 * case is a very large file with very small lines, in which case the index file can be several times larger than the
 * original file.  The index file is created in the same directory as the file itself and with a filename suffix of
 * .index.
 */
public class LineReaderWithFileIndex extends LineReader {

    /**
     * The fixed width of each line in the indexed file.  Note this is inclusive of the newline character.  Therefore
     * the longest entry that can be written to the indexed file is a 10 digit number.  This is chosen because {@link
     * Integer#MAX_VALUE} = 2,147,483,647 which is 10 digits.
     */
    public static final int INDEX_ENTRY_FIXED_WIDTH = 11;

    /**
     * Suffix appended to the original text file to determine the path of the index file.
     */
    public static final String INDEX_FILENAME_SUFFIX = ".index";

    /**
     * Number of lines in the text file.
     */
    private Integer lineCount = null;

    /**
     * Construct a line reader.  Read the entire file and build an index containing the position of all newline
     * characters in the original file.  This can take several minutes for a very large file.
     *
     * @param f
     */
    public LineReaderWithFileIndex(File f) {
        super(f);

        initIndex();
    }

    @Override
    public String getLine(int lineNumber) throws IOException {
        validateLineNumber(lineNumber);
        return FileUtils.getFromFile(textFile, getStartPosFromIndex(lineNumber), getEndPosFromIndex(lineNumber));
    }

    /**
     * Calculate the starting position of the requested line number by consulting the index.
     *
     * @param lineNumber the requested line number, 1-indexed
     * @return
     * @throws IOException
     */
    private long getStartPosFromIndex(int lineNumber) throws IOException {
        long start = 0;
        if (lineNumber == 1) {
            start = 0;
        } else {
            // Subtract 2 because the API for retrieving line numbers is 1 indexed.
            start = getIndexEntry(lineNumber - 2) + 1;
        }
        return start;
    }

    /**
     * Calculate the ending position of the requested line number by consulting the index.
     *
     * @param lineNumber the requested line number, 1-indexed
     * @return
     * @throws IOException
     */
    private long getEndPosFromIndex(int lineNumber) throws IOException {
        // Subtract 1 because the API for retrieving line numbers is 1 indexed.
        return getIndexEntry(lineNumber - 1);
    }

    /**
     * Extract the requested line from the index and parse it into an integer.
     *
     * @param n The requested entry in the index file, zero-indexed.
     * @return The position of the nth newline character in the original text file.
     * @throws IOException
     */
    private long getIndexEntry(int n) throws IOException {
        // Index line positions can be easily accessed because of fixed width.
        final String indexFileLine = FileUtils.getFromFile(getIndexFile(),
                n * INDEX_ENTRY_FIXED_WIDTH,
                (n + 1) * INDEX_ENTRY_FIXED_WIDTH);
        return Long.parseLong(indexFileLine.trim());
    }

    public Integer getLineCount() {
        if (lineCount == null) {
            lineCount = (int) (getIndexFile().length() / INDEX_ENTRY_FIXED_WIDTH);
        }
        return lineCount;
    }

    /**
     * Create an index file recording the position of each newline.  File is sorted, and each line is exactly 16
     * characters:  15 for a representation of the number and 1 for the newline.
     */
    public void initIndex() {
        lineCount = 0;
        long pos = 0;
        long lastPos = 0;
        final String format = "%-" + (INDEX_ENTRY_FIXED_WIDTH - 1) + "d\n";

        File index = getIndexFile();
        try (InputStream in = new BufferedInputStream(new FileInputStream(textFile));
             FileWriter writer = new FileWriter(index)) {
            int c;
            while ((c = in.read()) >= 0) {
                if (c == '\n') {
                    writer.write(String.format(format, pos));
                    lastPos = pos;
                    lineCount++;
                }
                pos++;
            }
            // Treat EOF like a newline if not preceded immediately by a newline.
            if (pos > 0 && pos != lastPos + 1) {
                writer.write(String.format(format, pos));
                lineCount++;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get the index file.  This is the same as the original file with a filename suffix of ".index".
     *
     * @return
     */
    private File getIndexFile() {
        return new File(textFile.getParent(), textFile.getName() + INDEX_FILENAME_SUFFIX);
    }
}
