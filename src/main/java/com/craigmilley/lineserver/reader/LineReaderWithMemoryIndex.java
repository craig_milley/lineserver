package com.craigmilley.lineserver.reader;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * LineReader which reads the entire file at construction time and builds an in-memory index of the positions of all
 * newline characters in the file.  At request time, uses {@link java.io.RandomAccessFile} API to extract the requested
 * line from the file.
 * <p/>
 * NOTE:  This implementation class is only suitable for relatively small files since the list of integers can exhaust
 * memory if the file is very large.
 */
public class LineReaderWithMemoryIndex extends LineReader {

    /**
     * List containing all the positions of newline characters within the text file.
     */
    private List<Long> positions = new ArrayList<>();

    /**
     * Construct the line reader.  Read entire file at construction time, and record the positions of all newline
     * characters.
     *
     * @param textFile
     */
    public LineReaderWithMemoryIndex(File textFile) {
        super(textFile);
        initIndex();
    }

    @Override
    public Integer getLineCount() {
        return positions.size();
    }

    @Override
    public String getLine(int lineNumber) throws IOException {
        validateLineNumber(lineNumber);

        // Determine start and end position of line within the file.
        long lineBegin = lineNumber == 1 ? 0 : (1 + positions.get(lineNumber - 2));
        long lineEnd = positions.get(lineNumber - 1);

        if (lineEnd == (lineBegin + 1)) {
            // short circuit
            return "";
        }

        // Get the line.
        return FileUtils.getFromFile(textFile, lineBegin, lineEnd);
    }

    /**
     * Read the entire file.  Record the position of all encountered newline characters.
     */
    private void initIndex() {
        long pos = 0;
        try (InputStream in = new BufferedInputStream(new FileInputStream(textFile))) {
            int c;
            while ((c = in.read()) >= 0) {
                if (c == '\n') {
                    positions.add(pos);
                }
                pos++;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Treat EOF like a newline if it is not preceded immediately by a newline
        if (pos > 0 && !positions.contains(pos - 1)) {
            positions.add(pos);
        }
    }
}
